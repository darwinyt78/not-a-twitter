import PostFeed from "@/components/posts/PostFeed";
import Header from "@/components/Header";
import Form from "@/components/Form";
import Head from "next/head";

const pageTitle = "Головна";

export default function Home() {
  return (
    <>
      <Head>
        <title>{pageTitle}</title>
      </Head>
      <Header label="Головна" />
      <Form placeholder="На кого скаржитися будемо?" />
      <PostFeed />
    </>
  );
}
